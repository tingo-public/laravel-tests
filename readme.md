# Laravel Tests

* PHP environment for SQLite based tests
* CI image build on GitLab

## PHP 8.0
registry.gitlab.com/tingo-public/laravel-tests/php-8.0

## PHP 8.1
registry.gitlab.com/tingo-public/laravel-tests/php-8.1